import React, { FC, useCallback, useMemo, useState } from 'react';
import { useLocalStorage } from 'usehooks-ts';
import {
  AdditionalData,
  ContactsData,
  MigrantData,
  ParentsData,
  PassportData,
  RepresentativeData, UserData,
  WorkingContractData,
} from '../types';
import {
  DEFAULT_ADDITIONAL_DATA,
  DEFAULT_CONTACTS_DATA,
  DEFAULT_MIGRANT_DATA,
  DEFAULT_PARENTS_DATA,
  DEFAULT_PASSPORT_DATA, DEFAULT_REPRESENTATIVE_DATA, DEFAULT_WORKING_CONTRACT_DATA,
} from './Forms/constants';
import ReactDOM from 'react-dom/server';
import { Anexo } from './Documents/Anexo';
import { Button, Descriptions, DescriptionsProps, Result } from 'antd';
import { FormStepProps } from './Forms/types';
import { DeclarationRemoteWork } from './Documents/DeclarationRemoteWork';
import dayjs from 'dayjs';
import {
  BRAZILIAN_STATES,
  EDUCATION_LEVELS_EN,
  FAMILY_STATUSES_EN,
  GENDERS_EN,
  MIGRATION_STATUSES_EN, OCCUPATIONS_EN,
} from '../constants';
import { DeclarationCriminal } from './Documents/DeclarationCriminal';

export const Download: FC<FormStepProps> = ({ handlePreviousStep }) => {
  const [migrantData] = useLocalStorage<MigrantData>('migrantData', DEFAULT_MIGRANT_DATA);
  const [parentsData] = useLocalStorage<ParentsData>('parentsData', DEFAULT_PARENTS_DATA);
  const [contactsData] = useLocalStorage<ContactsData>('contactsData', DEFAULT_CONTACTS_DATA);
  const [passportData] = useLocalStorage<PassportData>('passportData', DEFAULT_PASSPORT_DATA);
  const [workingContractData] = useLocalStorage<WorkingContractData>('workingContractData', DEFAULT_WORKING_CONTRACT_DATA);
  const [representativeData] = useLocalStorage<RepresentativeData>('representativeData', DEFAULT_REPRESENTATIVE_DATA);
  const [additionalData] = useLocalStorage<AdditionalData>('additionalData', DEFAULT_ADDITIONAL_DATA);

  const [loading, setLoading] = useState(false);

  const userData: UserData = useMemo(() => ({
    migrantData,
    parentsData,
    contactsData,
    passportData,
    workingContractData,
    representativeData,
    additionalData
  }), [migrantData, parentsData, contactsData, passportData, workingContractData, representativeData, additionalData]);

  const download = useCallback(async () => {
    const anexo = ReactDOM.renderToString(<Anexo data={userData}/>);
    const declarationRemoteWork = ReactDOM.renderToString(<DeclarationRemoteWork data={userData}/>);
    const declarationCriminal = ReactDOM.renderToString(<DeclarationCriminal data={userData}/>);

    const newWindowObject = window as any;
    const html2pdf = newWindowObject.html2pdf;

    const opt = {
      margin: 0.5,
      filename: 'docs.pdf',
      image: { type: 'jpeg', quality: 1 },
      html2canvas: { scale: 5, scrollY: 0 },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
    };
    setLoading(true);
    await html2pdf().set(opt).from(`
      <!DOCTYPE html>
      <html lang="en">
      <head><meta charset="UTF-8"><title>Title</title></head>
      <body style="margin: 0; padding: 0">
        ${anexo}
        <div style="clear: both; page-break-after: always"></div>
        ${declarationRemoteWork}
        <div style="clear: both; page-break-after: always"></div>
        ${declarationCriminal}
      </body>
      </html>`).save();
    setLoading(false);
  }, [userData]);

  const items: DescriptionsProps['items'] = useMemo(() => [
    {
      key: '1',
      label: 'Full name',
      children: userData.migrantData.firstName + ' ' + userData.migrantData.lastName,
      span: { xs: 2, md: 1 },
    },
    {
      key: '2',
      label: 'Birth date',
      children: typeof userData.migrantData.birthDate === 'string' ? dayjs(userData.migrantData.birthDate, 'YYYY-MM-DD').format('DD.MM.YYYY') : userData.migrantData.birthDate.format('DD.MM.YYYY'),
      span: { xs: 2, md: 1 },
    },
    {
      key: '3',
      label: 'Place of birth',
      children: userData.migrantData.placeOfBirth,
      span: { xs: 2, md: 1 },
    },
    {
      key: '4',
      label: 'Gender',
      children: GENDERS_EN[userData.migrantData.gender],
      span: { xs: 2, md: 1 },
    },
    {
      key: '5',
      label: 'Family status',
      children: FAMILY_STATUSES_EN[userData.migrantData.familyStatus],
      span: { xs: 2, md: 1 },
    },
    {
      key: '6',
      label: 'Education level',
      children: EDUCATION_LEVELS_EN[userData.migrantData.educationLevel],
      span: { xs: 2, md: 1 },
    },
    {
      key: '7',
      label: 'Migration status',
      children: MIGRATION_STATUSES_EN[userData.migrantData.migrationStatus],
      span: { xs: 2, md: 1 },
    },
    {
      key: '8',
      label: 'CPF',
      children: userData.migrantData.cpf,
      span: { xs: 2, md: 1 },
    },
    {
      key: '9',
      label: 'Mother\'s full name',
      children: userData.parentsData.motherFullName,
      span: { xs: 2, md: 1 },
    },
    {
      key: '10',
      label: 'Father\'s full name',
      children: userData.parentsData.fatherFullName,
      span: { xs: 2, md: 1 },
    },
    {
      key: '11',
      label: 'Address',
      children: [userData.contactsData.zipCode, (BRAZILIAN_STATES as any)[userData.contactsData.state], userData.contactsData.city, userData.contactsData.address].join(', '),
      span: { xs: 2, sm: 2, md: 2, lg: 2, xl: 2, xxl: 2 },
    },
    {
      key: '12',
      label: 'E-mail',
      children: userData.contactsData.email,
      span: { xs: 2, md: 1 },
    },
    {
      key: '13',
      label: 'Phone',
      children: userData.contactsData.phone,
      span: { xs: 2, md: 1 },
    },
    {
      key: '14',
      label: 'Cell phone',
      children: userData.contactsData.cellPhone,
      span: { xs: 2, md: 1 },
    },
    {
      key: '15',
      label: 'Nationality',
      children: userData.passportData.nationality,
      span: { xs: 2, md: 1 },
    },
    {
      key: '16',
      label: 'Passport number',
      children: userData.passportData.passportNumber,
      span: { xs: 2, md: 1 },
    },
    {
      key: '17',
      label: 'Passport expiration date',
      children: typeof userData.passportData.passportExpirationDate === 'string' ? dayjs(userData.passportData.passportExpirationDate, 'YYYY-MM-DD').format('DD.MM.YYYY') : userData.passportData.passportExpirationDate.format('DD.MM.YYYY'),
      span: { xs: 2, md: 1 },
    },
    {
      key: '18',
      label: 'Occupation',
      children: userData.workingContractData.occupation === 'other' ? userData.workingContractData.otherOccupation : OCCUPATIONS_EN[userData.workingContractData.occupation],
      span: { xs: 2, md: 1 },
    },
    {
      key: '19',
      label: 'Salary',
      children: userData.workingContractData.salary + ' R$',
      span: { xs: 2, md: 1 },
    },
    {
      key: '20',
      label: 'Representative',
      children: `${userData.representativeData.representativeFullName}, CPF: ${userData.representativeData.representativeCpf}, E-mail: ${userData.representativeData.representativeEmail}`,
      span: { xs: 2, sm: 2, md: 2, lg: 2, xl: 2, xxl: 2 },
    },
    {
      key: '21',
      label: 'Purpose of obtaining a visa',
      children: userData.additionalData.purpose,
      span: { xs: 2, sm: 2, md: 2, lg: 2, xl: 2, xxl: 2 },
    },
  ], [userData]);

  return <Result
    status="success"
    title="Your application successfully generated"
    subTitle={
      <div>
        <span>Download, print, sign, scan and upload it</span>
        <Descriptions size={'small'} column={{ xs: 2, sm: 2, md: 2, lg: 2, xl: 2, xxl: 2 }} title="Your data" items={items} />
      </div>
    }
    extra={[
      <Button type="default" key="console" onClick={handlePreviousStep}>Change data</Button>,
      <Button disabled={loading} type="primary" key="buy" onClick={download}>Download</Button>,
    ]}
  />;
}