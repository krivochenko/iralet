import React, { useMemo, useState } from 'react';
import { MigrantForm } from './Forms/MigrantForm';
import { PassportForm } from './Forms/PassportForm';
import { ParentsForm } from './Forms/ParentsForm';
import { ContactsForm } from './Forms/ContactsForm';
import { WorkingContractForm } from './Forms/WorkingContractForm';
import { RepresentativeForm } from './Forms/RepresentativeForm';
import { AdditionalForm } from './Forms/AdditionalForm';
import { Download } from './Download';
import { Col, Grid, Row, Steps } from 'antd';

const { useBreakpoint } = Grid;

export const ModalForm = () => {
  const [current, setCurrent] = useState(0);
  const screen = useBreakpoint();
  const smallScreen = (screen.xs || screen.sm) && !screen.md;

  const steps = useMemo(() => {
    const next = () => setCurrent(old => old + 1);
    const previous = () => setCurrent(old => old - 1);
    return [
      {
        title: smallScreen ? '' : 'Migrant',
        content: <MigrantForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Passport',
        content: <PassportForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Parents',
        content: <ParentsForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Contacts',
        content: <ContactsForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Working contract',
        content: <WorkingContractForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Representative',
        content: <RepresentativeForm handleNextStep={next} handlePreviousStep={previous} />,
      },
      {
        title: smallScreen ? '' : 'Additional',
        content: <AdditionalForm handleNextStep={next} handlePreviousStep={previous} />
      },
      {
        title: smallScreen ? '' : 'Result',
        content: <Download handleNextStep={next} handlePreviousStep={previous} />,
      },
    ];
  }, [smallScreen]);

  return <div className={'Form'}>
    <Row gutter={[10, 10]}>
      <Col xs={24} md={6}>
        <Steps current={current} items={steps} responsive={false} direction={smallScreen ? 'horizontal' : 'vertical'} size={'small'} />
      </Col>
      <Col xs={24} md={18}>
        {steps[current].content}
      </Col>
    </Row>
  </div>;
};