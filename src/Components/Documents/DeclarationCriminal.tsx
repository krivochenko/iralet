import { FC } from 'react';
import { UserData } from '../../types';
import dayjs from 'dayjs';
import { FAMILY_STATUSES_PT } from '../../constants';
import { Gender } from '../../enums';

export const DeclarationCriminal: FC<{ data: UserData }> = ({ data }) => {
  return <div className={'Document'}>
    <h2>
      <b>DECLARAÇÃO</b>
      <br/>
      <span style={{ fontWeight: 100, fontSize: 14 }}>(A declaração deverá ser preenchida em letra de forma legível)</span>
    </h2>
    <div style={{ lineHeight: 2 }}>
      Eu, <u>{`${data.migrantData.firstName} ${data.migrantData.lastName}`}</u>,
      de nacionalidade <u>{data.passportData.nationality}</u>, estado
      civil <u>{FAMILY_STATUSES_PT[data.migrantData.familyStatus].toLowerCase()}</u>,&nbsp;
      {data.migrantData.gender === Gender.Male ? 'nascido' : 'nascida'} aos <u>{typeof data.migrantData.birthDate === 'string' ? dayjs(data.migrantData.birthDate, 'YYYY-MM-DD').format('DD/MM/YYYY') : data.migrantData.birthDate.format('DD/MM/YYYY')}</u>,
      na cidade de <u>{data.migrantData.placeOfBirth}</u>,
      filho de <u>{data.parentsData.motherFullName}</u> e
      de <u>{data.parentsData.fatherFullName}</u>, DECLARO, SOB AS PENAS DA LEI, QUE NÃO RESPONDO E NEM RESPONDI
      A INQUÉRITO POLICIAL, NEM A PROCESSO CRIMINAL, NEM SOFRI
      CONDENAÇÃO JUDICIAL, NO BRASIL E NO EXTERIOR NOS CINCO ANOS
      ANTERIORES À DATA DA SOLICITAÇÃO.
      <br/>
      <br/>
      <b>
        É considerado crime, com pena de reclusão e multa, omitir, em documento público
        ou particular, declaração que dele devia constar, ou nele inserir ou fazer inserir
        declaração falsa ou diversa da que devia ser escrita, com o fim de prejudicar direito,
        criar obrigação ou alterar a verdade sobre fato juridicamente relevante (Art. 299,
        do <a href={'https://www.planalto.gov.br/ccivil_03/decreto-lei/del2848compilado.htm'}>Código Penal</a>).
      </b>
    </div>
    <div style={{ marginTop: 100, marginBottom: 100, display: 'flex', justifyContent: 'center' }}>
      <div style={{ textAlign: 'center', margin: '0 50px' }}>
        <div style={{ padding: '0 10px' }}>{data.contactsData.city}, {data.contactsData.state}</div>
        <div style={{ display: 'block', height: 1, backgroundColor: 'black' }}></div>
        <div style={{ padding: '0 10px' }}>Cidade/UF</div>
      </div>
      <div style={{ textAlign: 'center', margin: '0 50px' }}>
        <div style={{ padding: '0 10px' }}>{dayjs().format('DD / MM / YYYY')}</div>
        <div style={{ display: 'block', height: 1, backgroundColor: 'black' }}></div>
        <div style={{ padding: '0 10px' }}>Data (dia, mês, ano)</div>
      </div>
    </div>
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <div style={{ textAlign: 'center', margin: '0 50px' }}>
        <div style={{ padding: '0 10px' }}></div>
        <div style={{ display: 'block', height: 1, backgroundColor: 'black' }}></div>
        <div style={{ padding: '0 10px' }}>Assinatura do Declarante</div>
      </div>
    </div>
  </div>;
}