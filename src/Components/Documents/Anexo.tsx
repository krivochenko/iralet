import { FC } from 'react';
import { UserData } from '../../types';
import {
  EDUCATION_LEVELS_PT,
  FAMILY_STATUSES_PT,
  GENDERS_PT,
  MIGRATION_STATUSES_PT,
  OCCUPATIONS_PT,
} from '../../constants';
import dayjs from 'dayjs';

export const Anexo: FC<{ data: UserData }> = ({ data }) => {
  return <div className={'Document'}>
    <h2>ANEXO I</h2>
    <h3>FORMULÁRIO DE REQUERIMENTO DE AUTORIZAÇÃO DE RESIDÊNCIA COM FUNDAMENTO NA RESOLUÇÃO CNIG MJSP No 45, DE 2021</h3>
    <table className={'bordered'}>
      <tbody>
      <tr>
        <td colSpan={7}>DADOS DO IMIGRANTE</td>
      </tr>
      <tr>
        <td colSpan={7}>
          Nome:&nbsp;&nbsp;
          <em>{data.migrantData.firstName} {data.migrantData.lastName}</em>
        </td>
      </tr>
      <tr>
        <td rowSpan={2}>Filiação</td>
        <td>Pai:</td>
        <td colSpan={5}>
          <em>{data.parentsData.fatherFullName}</em>
        </td>
      </tr>
      <tr>
        <td>Mãe:</td>
        <td colSpan={5}>
          <em>{data.parentsData.motherFullName}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Data de Nascimento:</td>
        <td>
          <em>{typeof data.migrantData.birthDate === 'string' ? dayjs(data.migrantData.birthDate, 'YYYY-MM-DD').format('DD.MM.YYYY') : data.migrantData.birthDate.format('DD.MM.YYYY')}</em>
        </td>
        <td>Sexo:</td>
        <td>
          <em>
            {GENDERS_PT[data.migrantData.gender]}
          </em>
        </td>
        <td>Estado civil:</td>
        <td>
          <em>{FAMILY_STATUSES_PT[data.migrantData.familyStatus]}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Escolaridade:</td>
        <td>
          <em>{EDUCATION_LEVELS_PT[data.migrantData.educationLevel]}</em>
        </td>
        <td>Profissão:</td>
        <td>
          <em>
            {data.workingContractData.occupation === 'other' ? data.workingContractData.otherOccupation : OCCUPATIONS_PT[data.workingContractData.occupation]}
          </em>
        </td>
        <td>CPF:</td>
        <td style={{ width: 130 }}>
          <em>{data.migrantData.cpf}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Nacionalidade:</td>
        <td>
          <em>{data.passportData.nationality}</em>
        </td>
        <td>Passaporte:</td>
        <td>
          <em>{data.passportData.passportNumber}</em>
        </td>
        <td>Validade:</td>
        <td>
          <em>{typeof data.passportData.passportExpirationDate === 'string' ? dayjs(data.passportData.passportExpirationDate, 'YYYY-MM-DD').format('DD.MM.YYYY') : data.passportData.passportExpirationDate.format('DD.MM.YYYY')}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Endereço</td>
        <td colSpan={5}>
          <em>{data.contactsData.address}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Cidade:</td>
        <td>
          <em>{data.contactsData.city}</em>
        </td>
        <td>UF:</td>
        <td>
          <em>{data.contactsData.state}</em>
        </td>
        <td>CEP:</td>
        <td>
          <em>{data.contactsData.zipCode}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Telefone:</td>
        <td>
          <em>{data.contactsData.phone}</em>
        </td>
        <td>Celular:</td>
        <td colSpan={5}>
          <em>{data.contactsData.cellPhone}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Remuneração mensal recebida no exterior em moeda nacional (Real):</td>
        <td colSpan={5}>
          <em>{data.workingContractData.salary}</em>
        </td>
      </tr>
      <tr>
        <td colSpan={2}>Situação migratória atual do imigrante:</td>
        <td colSpan={5}>
          {
            Object.keys(MIGRATION_STATUSES_PT).map((key) => {
              return <div key={key} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <input type={'radio'} checked={data.migrantData.migrationStatus === parseInt(key)} readOnly/>
                <span>{(MIGRATION_STATUSES_PT as any)[key]}</span>
              </div>;
            })
          }
        </td>
      </tr>
      </tbody>
    </table>
    <br/>
    <table className={'bordered'}>
      <tbody>
      <tr>
        <td style={{ textAlign: 'center' }} colSpan={3}>DADOS DO REPRESENTANTE LEGAL (preencher quando se fizer representar por procurador)</td>
      </tr>
      <tr>
        <td>Nome:</td>
        <td colSpan={2}>
          <em>
            {data.representativeData.representativeFullName}
          </em>
        </td>
      </tr>
      <tr>
        <td>E-mail</td>
        <td>
          <em>{data.representativeData.representativeEmail}</em>
        </td>
        <td>
          CPF: &nbsp;
          <em>{data.representativeData.representativeCpf}</em>
        </td>
      </tr>
      </tbody>
    </table>
    <br/>
    <table className={'bordered'}>
      <tbody>
      <tr>
        <td style={{ textAlign: 'center' }}>JUSTIFICATIVA</td>
      </tr>
      <tr>
        <td>
          {data.additionalData.purpose ? <em>{data.additionalData.purpose}</em> : <br/>}
          </td>
      </tr>
      </tbody>
    </table>
    <br/>
    <table className={'bordered'}>
      <tbody>
      <tr>
        <td style={{ textAlign: 'center'}}>TERMO DE RESPONSABILIDADE</td>
      </tr>
      <tr>
        <td>
          Declaro, sob as penas do art. 299 do Código Penal Brasileiro, serem verdadeiras as informações transcritas
          neste documento, comprometendo-me, inclusive, a comprová-las, mediante a apresentação dos documentos próprios
          à fiscalização.<br/>
          (LOCAL E DATA)<br/>
          Assinatura do requerente ou de seu representante legal, discriminando-se o nome completo e CPF.<br/>
        </td>
      </tr>
      </tbody>
    </table>
    <br/>
    <table style={{ textAlign: 'right', width: '100%' }}>
      <tbody>
        <tr>
          <td></td>
          <td>
            <em>
              {data.contactsData.city}, {data.contactsData.state}
            </em>
          </td>
          <td>
            <em>
              {dayjs().format('DD/MM/YYYY')}
            </em>
          </td>
        </tr>
        <tr>
          <td style={{ borderTop: 'solid 1px black', textAlign: 'center' }}>Assinatura</td>
          <td>
            <em>{data.migrantData.firstName} {data.migrantData.lastName}</em>
          </td>
          <td>
            <em>CPF:&nbsp;{data.migrantData.cpf}</em>
          </td>
        </tr>
      </tbody>
    </table>
  </div>;
};