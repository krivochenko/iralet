import { WorkingContractData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Button, Col, Form, Input, InputNumber, Row, Select } from 'antd';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { FormStepProps } from './types';
import { OCCUPATIONS_EN } from '../../constants';
import { DEFAULT_WORKING_CONTRACT_DATA } from './constants';

export const WorkingContractForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [workingContractData, setWorkingContractData] = useLocalStorage<WorkingContractData>('workingContractData', DEFAULT_WORKING_CONTRACT_DATA);

  const [form] = Form.useForm<WorkingContractData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setWorkingContractData(formValues);
    }
  }, [formValues, setWorkingContractData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  const occupationOptions = useMemo(() => {
    return Object.keys(OCCUPATIONS_EN).map((value) => ({ value, label: OCCUPATIONS_EN[value] }));
  }, []);

  return <div>
    <Form initialValues={workingContractData} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24}>
          <Form.Item<WorkingContractData>
            label="Occupation"
            name="occupation"
            rules={[{ required: true, message: 'Please select your occupation!' }]}
          >
            <Select options={occupationOptions}/>
          </Form.Item>
        </Col>
        {
          formValues && formValues.occupation === 'other'
            ? <Col xs={24}>
              <Form.Item<WorkingContractData>
                label="Occupation name"
                name="otherOccupation"
                rules={[{ required: true, message: 'Please input your occupation!' }]}
              >
                <Input/>
              </Form.Item>
            </Col>
            : null
        }
        <Col xs={24}>
          <Form.Item<WorkingContractData>
            label="Salary (in Brazilian Reais)"
            name="salary"
            rules={[{ required: true, message: 'Please input your salary!' }]}
          >
            <InputNumber style={{ display: 'block', width: '100%' }}/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};