import { MigrantData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Button, Col, DatePicker, Form, Input, Row, Select } from 'antd';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { MaskedInput } from 'antd-mask-input';
import dayjs from 'dayjs';
import { FormStepProps } from './types';
import { DEFAULT_MIGRANT_DATA } from './constants';
import { EDUCATION_LEVELS_EN, FAMILY_STATUSES_EN, GENDERS_EN, MIGRATION_STATUSES_EN } from '../../constants';

export const MigrantForm: FC<FormStepProps> = ({ handleNextStep }) => {
  const [migrantData, setMigrantData] = useLocalStorage<MigrantData>('migrantData', DEFAULT_MIGRANT_DATA);

  const formInitialValues = useMemo(() => ({
    ...migrantData,
    birthDate: typeof migrantData.birthDate === 'string' ? dayjs(migrantData.birthDate, 'YYYY-MM-DD') : dayjs(),
  }), [migrantData]);
  const [form] = Form.useForm<MigrantData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setMigrantData({ ...formValues, birthDate: typeof formValues.birthDate === 'string' ? formValues.birthDate : formValues.birthDate.format('YYYY-MM-DD') });
    }
  }, [formValues, setMigrantData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  const genderOptions = useMemo(() => {
    return Object.keys(GENDERS_EN).map((value) => ({ value: parseInt(value), label: (GENDERS_EN as any)[value] }));
  }, []);

  const familyStatusOptions = useMemo(() => {
    return Object.keys(FAMILY_STATUSES_EN).map((value) => ({ value: parseInt(value), label: (FAMILY_STATUSES_EN as any)[value] }));
  }, []);

  const educationLevelOptions = useMemo(() => {
    return Object.keys(EDUCATION_LEVELS_EN).map((value) => ({ value: parseInt(value), label: (EDUCATION_LEVELS_EN as any)[value] }));
  }, []);

  const migrationStatusOptions = useMemo(() => {
    return Object.keys(MIGRATION_STATUSES_EN).map((value) => ({ value: parseInt(value), label: (MIGRATION_STATUSES_EN as any)[value] }));
  }, []);

  return <div>
    <Form initialValues={formInitialValues} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label="First name"
            name="firstName"
            rules={[{ required: true, message: 'Please input your first name!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label="Last name"
            name="lastName"
            rules={[{ required: true, message: 'Please input your last name!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label="Birth date"
            name="birthDate"
            rules={[{ required: true, message: 'Please input your birth date!' }]}
          >
            <DatePicker style={{ display: 'block' }}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label="Place of birth"
            name="placeOfBirth"
            rules={[{ required: true, message: 'Please input place of your birth!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<MigrantData>
            label={'Gender'}
            name={'gender'}
            rules={[{ required: true, message: 'Please select your gender!' }]}
          >
            <Select options={genderOptions}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<MigrantData>
            label={'Family status'}
            name={'familyStatus'}
            rules={[{ required: true, message: 'Please select your family status!' }]}
          >
            <Select options={familyStatusOptions}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<MigrantData>
            label={'Education level'}
            name={'educationLevel'}
            rules={[{ required: true, message: 'Please select your education level!' }]}
          >
            <Select options={educationLevelOptions}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label={'Migration status'}
            name={'migrationStatus'}
            rules={[{ required: true, message: 'Please select your migration status!' }]}
          >
            <Select options={migrationStatusOptions}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<MigrantData>
            label="CPF"
            name="cpf"
            rules={[{ required: true, message: 'Please input your CPF!', pattern: /^\d{3}\.\d{3}\.\d{3}-\d{2}$/ }]}
          >
            <MaskedInput mask={'000.000.000-00'}/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};