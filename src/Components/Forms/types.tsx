export type FormStepProps = {
  handleNextStep: () => void;
  handlePreviousStep: () => void;
}