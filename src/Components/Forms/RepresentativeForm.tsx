import { RepresentativeData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Alert, Button, Col, Form, Input, Row } from 'antd';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { FormStepProps } from './types';
import { MaskedInput } from 'antd-mask-input';
import { DEFAULT_REPRESENTATIVE_DATA } from './constants';

export const RepresentativeForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [representativeData, setRepresentativeData] = useLocalStorage<RepresentativeData>('representativeData', DEFAULT_REPRESENTATIVE_DATA);
  const [form] = Form.useForm<RepresentativeData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setRepresentativeData(formValues);
    }
  }, [formValues, setRepresentativeData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  const rules = useMemo(() => {
    return {
      representativeFullName: [],
      representativeEmail: formValues && formValues.representativeFullName ? [{ required: true, type: 'email', message: 'Please input your e-mail of representative!' }] : [],
      representativeCpf: formValues && formValues.representativeFullName ? [{ required: true, message: 'Please input CPF of representative!', pattern: /^\d{3}\.\d{3}\.\d{3}-\d{2}$/ }] : [],
      justification: formValues && formValues.representativeFullName ? [{ required: true, message: 'Please input justification!' }] : [],
    };
  }, [formValues]);

  return <div style={{ width: '100%' }}>
    <Alert message="Fields on this step required only if there's a representative of migrante" type="info" style={{ marginBottom: 20 }} />
    <Form initialValues={representativeData} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24}>
          <Form.Item<RepresentativeData>
            label="Representative's full name"
            name="representativeFullName"
            rules={rules.representativeFullName}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<RepresentativeData>
            label="Representative's e-mail"
            name="representativeEmail"
            rules={rules.representativeEmail as any}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<RepresentativeData>
            label="Representative's CPF"
            name="representativeCpf"
            rules={rules.representativeCpf as any}
          >
            <MaskedInput mask={'000.000.000-00'}/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};