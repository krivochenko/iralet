import { PassportData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Button, Col, DatePicker, Form, Input, Row } from 'antd';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { FormStepProps } from './types';
import dayjs from 'dayjs';
import { DEFAULT_PASSPORT_DATA } from './constants';

export const PassportForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [passportData, setPassportData] = useLocalStorage<PassportData>('passportData', DEFAULT_PASSPORT_DATA);
  const formInitialValues = useMemo(() => ({
    ...passportData,
    passportExpirationDate: typeof passportData.passportExpirationDate === 'string' ? dayjs(passportData.passportExpirationDate, 'YYYY-MM-DD') : dayjs(),
  }), [passportData]);
  const [form] = Form.useForm<PassportData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setPassportData({ ...formValues, passportExpirationDate: typeof formValues.passportExpirationDate === 'string' ? formValues.passportExpirationDate : formValues.passportExpirationDate.format('YYYY-MM-DD') });
    }
  }, [formValues, setPassportData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  return <div style={{ width: '100%' }}>
    <Form initialValues={formInitialValues} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24} sm={12}>
          <Form.Item<PassportData>
            label="Nationality"
            name="nationality"
            rules={[{ required: true, message: 'Please input your nationality!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<PassportData>
            label="Passport number"
            name="passportNumber"
            rules={[{ required: true, message: 'Please input your cell passport number!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={12}>
          <Form.Item<PassportData>
            label="Passport expiration date"
            name="passportExpirationDate"
            rules={[{ required: true, message: 'Please input your passport expiration date!' }]}
          >
            <DatePicker style={{ display: 'block' }}/>
          </Form.Item>
        </Col>

      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};