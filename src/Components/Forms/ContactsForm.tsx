import { ContactsData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Button, Col, Form, Input, Row, Select } from 'antd';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { FormStepProps } from './types';
import { DEFAULT_CONTACTS_DATA } from './constants';
import { BRAZILIAN_STATES } from '../../constants';

export const ContactsForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [contactsData, setContactsData] = useLocalStorage<ContactsData>('contactsData', DEFAULT_CONTACTS_DATA);
  const [form] = Form.useForm<ContactsData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setContactsData(formValues);
    }
  }, [formValues, setContactsData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  const stateOptions = useMemo(() => {
    return Object.keys(BRAZILIAN_STATES).map((state) => ({ value: state, label: (BRAZILIAN_STATES as any)[state] }));
  }, []);

  return <div style={{ width: '100%' }}>
    <Form initialValues={contactsData} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24} sm={8}>
          <Form.Item<ContactsData>
            label="Phone"
            name="phone"
            rules={[{ required: true, message: 'Please input your phone number!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<ContactsData>
            label="Cell phone"
            name="cellPhone"
            rules={[{ required: true, message: 'Please input your cell phone number!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<ContactsData>
            label="E-mail"
            name="email"
            rules={[{ required: true, message: 'Please input your e-mail number!', type: 'email' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={8}>
          <Form.Item<ContactsData>
            label="Zip-code"
            name="zipCode"
            rules={[{ required: true, message: 'Please input your zip-code!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={10}>
          <Form.Item<ContactsData>
            label="State"
            name="state"
            rules={[{ required: true, message: 'Please input your state!' }]}
          >
           <Select options={stateOptions}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={10}>
          <Form.Item<ContactsData>
            label="City"
            name="city"
            rules={[{ required: true, message: 'Please input your city!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24}>
          <Form.Item<ContactsData>
            label="Address"
            name="address"
            rules={[{ required: true, message: 'Please input your address!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};