import React, { FC, useEffect, useState } from 'react';
import { FormStepProps } from './types';
import { useLocalStorage } from 'usehooks-ts';
import { AdditionalData } from '../../types';
import { DEFAULT_ADDITIONAL_DATA } from './constants';
import { Button, Col, Form, Input, Row } from 'antd';

export const AdditionalForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [additionalData, setAdditionalData] = useLocalStorage<AdditionalData>('additionalData', DEFAULT_ADDITIONAL_DATA);
  const [form] = Form.useForm<AdditionalData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setAdditionalData(formValues);
    }
  }, [formValues, setAdditionalData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  return <div style={{ width: '100%' }}>
    <Form initialValues={additionalData} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24}>
          <Form.Item<AdditionalData>
            label="Purpose of obtaining a visa"
            name="purpose"
            rules={[{ required: true }]}
            help={'Provide a brief description of the purpose of obtaining a visa on Portuguese. For example: "Quero morar no Brasil legal".'}
          >
            <Input.TextArea rows={3}/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
}