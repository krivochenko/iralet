import dayjs from 'dayjs';
import { BrazilianStates, EducationLevel, FamilyStatus, Gender, MigrationStatus } from '../../enums';

export const DEFAULT_MIGRANT_DATA = { firstName: '', lastName: '', birthDate: dayjs().format('YYYY-MM-DD'), placeOfBirth: '', cpf: '', gender: Gender.Male, educationLevel: EducationLevel.High, familyStatus: FamilyStatus.Single, migrationStatus: MigrationStatus.Visitor };
export const DEFAULT_PARENTS_DATA = { fatherFullName: '', fatherCpf: '', motherFullName: '', motherCpf: '' };
export const DEFAULT_CONTACTS_DATA = { phone: '', email: '', address: '', cellPhone: '', city: '', state: BrazilianStates.AC, zipCode: '' };
export const DEFAULT_REPRESENTATIVE_DATA = { representativeFullName: '', representativeEmail: '', representativeCpf: '', justification: '' };
export const DEFAULT_PASSPORT_DATA = { nationality: '', passportNumber: '', passportExpirationDate: dayjs().format('YYYY-MM-DD')};
export const DEFAULT_WORKING_CONTRACT_DATA = { occupation: '', otherOccupation: '', salary: 0 };
export const DEFAULT_ADDITIONAL_DATA = { purpose: '' };