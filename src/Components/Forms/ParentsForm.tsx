import { ParentsData } from '../../types';
import { useLocalStorage } from 'usehooks-ts';
import { Button, Col, Form, Input, Row } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { FormStepProps } from './types';
import { DEFAULT_PARENTS_DATA } from './constants';

export const ParentsForm: FC<FormStepProps> = ({ handleNextStep, handlePreviousStep }) => {
  const [parentsData, setParentsData] = useLocalStorage<ParentsData>('parentsData', DEFAULT_PARENTS_DATA);
  const [form] = Form.useForm<ParentsData>();

  const formValues = Form.useWatch([], form);

  useEffect(() => {
    if (formValues) {
      setParentsData(formValues);
    }
  }, [formValues, setParentsData]);

  const [isValid, setIsValid] = useState<boolean>();
  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsValid(true))
      .catch(() => setIsValid(false));
  }, [form, formValues]);

  return <div style={{ width: '100%' }}>
    <Form initialValues={parentsData} autoComplete={'off'} form={form} layout={'vertical'}>
      <Row gutter={[10, 0]}>
        <Col xs={24}>
          <Form.Item<ParentsData>
            label="Father's full name"
            name="fatherFullName"
            rules={[{ required: true, message: 'Please input your full name of your father!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
        <Col xs={24}>
          <Form.Item<ParentsData>
            label="Mother's full name"
            name="motherFullName"
            rules={[{ required: true, message: 'Please input your full name of your mother!' }]}
          >
            <Input/>
          </Form.Item>
        </Col>
      </Row>
    </Form>
    <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
      <Button type={'default'} onClick={handlePreviousStep} style={{ marginRight: 10 }}>Go back</Button>
      <Button type={'primary'} disabled={!isValid} onClick={handleNextStep}>Continue</Button>
    </div>
  </div>;
};