import React, { useMemo, useState } from 'react';
import './App.scss';
import { Alert, Button, Grid, Modal, Steps } from 'antd';
import { ModalForm } from './Components/ModalForm';
import { FormOutlined, PrinterOutlined, SaveOutlined, ScanOutlined, SignatureOutlined } from '@ant-design/icons';

const { useBreakpoint } = Grid;

function App() {
  const screen = useBreakpoint();
  const smallScreen = (screen.xs || screen.sm) && !screen.md;
  const [isOpened, setIsOpened] = useState(false);

  const items = useMemo(() => [
    { icon: <FormOutlined />, title: smallScreen ? '' : 'Fill' },
    { icon: <PrinterOutlined />, title: smallScreen ? '' : 'Print' },
    { icon: <SignatureOutlined />, title: smallScreen ? '' : 'Sign' },
    { icon: <ScanOutlined />, title: smallScreen ? '' : 'Scan' },
    { icon: <SaveOutlined />, title: smallScreen ? '' : 'Save' },
  ], [smallScreen]);

  return <>
    <Steps current={5} items={items} responsive={false} direction={'horizontal'}/>
    <Alert description={'We do not send, store or share your personal data anywhere. All manipulations take place in your browser.'}/>
    <div style={{ display: 'flex', height: 100, alignItems: 'center', justifyContent: 'center' }}>
      <Button type={'primary'} onClick={() => setIsOpened(true)}>Start</Button>
    </div>
    <Modal open={isOpened} onCancel={() => setIsOpened(false)} footer={[]} width={800}>
      <ModalForm/>
    </Modal>
  </>;
}

export default App;
