import { BrazilianStates, EducationLevel, FamilyStatus, Gender, MigrationStatus } from './enums';

export const GENDERS_EN = {
  [Gender.Male]: 'Male',
  [Gender.Female]: 'Female',
};

export const GENDERS_PT = {
  [Gender.Male]: 'Masculino',
  [Gender.Female]: 'Feminino',
};

export const FAMILY_STATUSES_EN = {
  [FamilyStatus.Single]: 'Single',
  [FamilyStatus.Married]: 'Married',
  [FamilyStatus.Divorced]: 'Divorced',
  [FamilyStatus.Widowed]: 'Widowed',
};

export const FAMILY_STATUSES_PT = {
  [FamilyStatus.Single]: 'Solteiro',
  [FamilyStatus.Married]: 'Casado',
  [FamilyStatus.Divorced]: 'Divorciado',
  [FamilyStatus.Widowed]: 'Viúvo',
};

export const EDUCATION_LEVELS_EN = {
  [EducationLevel.None]: 'None',
  [EducationLevel.Elementary]: 'Elementary',
  [EducationLevel.Middle]: 'Middle',
  [EducationLevel.High]: 'High',
  [EducationLevel.University]: 'University',
};

export const EDUCATION_LEVELS_PT = {
  [EducationLevel.None]: 'Nenhum',
  [EducationLevel.Elementary]: 'Fundamental',
  [EducationLevel.Middle]: 'Médio',
  [EducationLevel.High]: 'Superior',
  [EducationLevel.University]: 'Universitário',
};

export const MIGRATION_STATUSES_EN = {
  [MigrationStatus.Visitor]: 'Visitor',
  [MigrationStatus.Diplomatic]: 'Diplomatic',
  [MigrationStatus.Visa]: 'Visa',
  [MigrationStatus.InNeedOfRegularization]: 'In need of regularization',
  [MigrationStatus.Other]: 'Other',
};

export const MIGRATION_STATUSES_PT = {
  [MigrationStatus.Visitor]: 'Visitante',
  [MigrationStatus.Diplomatic]: 'Portador de visto diplomático, oficial ou de cortesia',
  [MigrationStatus.Visa]: 'Portador de visto temporário/outra hipótese de Autorização de Residência',
  [MigrationStatus.InNeedOfRegularization]: 'Em necessidade de regularização no País',
  [MigrationStatus.Other]: 'Outra condição',
};

export const OCCUPATIONS_EN: {[key: string]: string} = {
  '2124-05': 'IT Developer',
  '3171-05': 'Web-developer',
  '2624-10': 'Web Designer',
  '2611-20': 'Web Editor',
  '1426-05': 'Development Manager (Technology)',
  '2124-05-1': 'Web Systems Analyst',
  '1425-10': 'Systems Development Manager/Software Development Manager',
  '1423-15': 'Marketing Division Manager',
  '1423-35': 'Marketing Analyst',
  '1423-05': 'Commercial Division Manager',
  '1426-05-1': 'Planning and New Projects Manager',
  '1422-05': 'Human Resources Manager',
  'other': 'Other',
};

export const OCCUPATIONS_PT: {[key: string]: string} = {
  '2124-05': 'Desenvolvedor de TI',
  '3171-05': 'Desenvolvedor web',
  '2624-10': 'Desenhista de páginas da internet',
  '2611-20': 'Editor de web',
  '1426-05': 'Gerente de desenvolvimento (tecnologia)',
  '2124-05-1': 'Analista de sistemas web',
  '1425-10': 'Gerente de desenvolvimento de sistemas/Gerente de desenvolvimento de software',
  '1423-15': 'Gerente de divisão de marketing',
  '1423-35': 'Analista de marketing',
  '1423-05': 'Gerente de divisão comercial',
  '1426-05-1': 'Gerente de planejamento e novos projetos',
  '1422-05': 'Gerente de recursos humanos',
  'other': 'Outra',
};

export const BRAZILIAN_STATES = {
  [BrazilianStates.AC]: 'Acre',
  [BrazilianStates.AL]: 'Alagoas',
  [BrazilianStates.AP]: 'Amapá',
  [BrazilianStates.AM]: 'Amazonas',
  [BrazilianStates.BA]: 'Bahia',
  [BrazilianStates.CE]: 'Ceará',
  [BrazilianStates.DF]: 'Distrito Federal',
  [BrazilianStates.ES]: 'Espírito Santo',
  [BrazilianStates.GO]: 'Goiás',
  [BrazilianStates.MA]: 'Maranhão',
  [BrazilianStates.MT]: 'Mato Grosso',
  [BrazilianStates.MS]: 'Mato Grosso do Sul',
  [BrazilianStates.MG]: 'Minas Gerais',
  [BrazilianStates.PA]: 'Pará',
  [BrazilianStates.PB]: 'Paraíba',
  [BrazilianStates.PR]: 'Paraná',
  [BrazilianStates.PE]: 'Pernambuco',
  [BrazilianStates.PI]: 'Piauí',
  [BrazilianStates.RJ]: 'Rio de Janeiro',
  [BrazilianStates.RN]: 'Rio Grande do Norte',
  [BrazilianStates.RS]: 'Rio Grande do Sul',
  [BrazilianStates.RO]: 'Rondônia',
  [BrazilianStates.RR]: 'Roraima',
  [BrazilianStates.SC]: 'Santa Catarina',
  [BrazilianStates.SP]: 'São Paulo',
  [BrazilianStates.SE]: 'Sergipe',
  [BrazilianStates.TO]: 'Tocantins',
}