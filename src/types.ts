import { BrazilianStates, EducationLevel, FamilyStatus, Gender, MigrationStatus } from './enums';
import dayjs from 'dayjs';

export type MigrantData = {
  firstName: string;
  lastName: string;
  birthDate: string | dayjs.Dayjs;
  placeOfBirth: string;
  gender: Gender;
  familyStatus: FamilyStatus;
  educationLevel: EducationLevel;
  migrationStatus: MigrationStatus;
  cpf: string;
};

export type ParentsData = {
  motherFullName: string;
  fatherFullName: string;
};

export type ContactsData = {
  address: string;
  city: string;
  state: BrazilianStates;
  zipCode: string;
  email: string;
  phone: string;
  cellPhone: string;
};

export type PassportData = {
  nationality: string;
  passportNumber: string;
  passportExpirationDate: string | dayjs.Dayjs;
};

export type WorkingContractData = {
  occupation: string;
  otherOccupation: string;
  salary: number | undefined;
};

export type RepresentativeData = {
  representativeFullName: string;
  representativeCpf: string;
  representativeEmail: string;
};

export type AdditionalData = {
  purpose: string;
}

export type UserData = {
  migrantData: MigrantData;
  parentsData: ParentsData;
  contactsData: ContactsData;
  passportData: PassportData;
  workingContractData: WorkingContractData;
  representativeData: RepresentativeData;
  additionalData: AdditionalData;
}
